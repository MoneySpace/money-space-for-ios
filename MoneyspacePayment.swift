

import UIKit
import SafariServices


class MoneyspacePayment{
    
    var base_url : String = "https://a.moneyspace.net"
    var secret_id : String
    var secret_key : String
    
    init(secret_id :String , secret_key :String) {
        self.secret_id = secret_id
        self.secret_key = secret_key
    }
    
    func CreateTransactionID (firstname : String, lastname : String, email : String , phone : String , amount: String , description : String , address : String , feeType : String , message : String , order_id : String , payment_type : String , success_Url : String , fail_Url : String , cancel_Url : String , res: @escaping (String) -> Void){
        
        var request = URLRequest(url: URL(string: "\(base_url)/payment/CreateTransaction")!)
            request.httpMethod = "POST"
        let postString = "secret_id=\(secret_id)&secret_key=\(secret_key)&firstname=\(firstname)&lastname=\(lastname)&email=\(email)&phone=\(phone)&amount=\(amount)&description=\(description)&address=\(address)&feeType=\(feeType)&message=\(message)&order_id=\(order_id)&payment_type=\(payment_type)&success_Url=\(success_Url)&fail_Url=\(fail_Url)&cancel_Url=\(cancel_Url)"
            request.httpBody = postString.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
                           
        guard let data = data, error == nil else {
            print("error=(error)")
            return
        }

        if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
            print("statusCode should be 200, but is \(httpStatus.statusCode)")
            print("response = \(String(describing: response))")

        }

        let responseString = String(data: data, encoding: .utf8)
        struct param : Codable {
                var status: String?
                var transaction_ID: String?
                var link_payment: URL?
                var image_qrprom: URL?
                private enum CodingKeys : String, CodingKey {
                    case status = "status"
                    case transaction_ID = "transaction_ID"
                    case link_payment = "link_payment"
                    case image_qrprom = "image_qrprom"
                }
        }
        let jsonData = responseString!.data(using: .utf8)!
        let users = try! JSONDecoder().decode([param].self, from: jsonData)
            for user in users {
                print(user.status)
                print(user.transaction_ID)
                if(user.link_payment != nil){
                    UIApplication.shared.open(user.link_payment!)
                }else if(user.image_qrprom != nil){
                    print(user.image_qrprom)
                }
            }
            
            res(responseString!)
           
        }
        task.resume()
        
    }
    
    func CheckTransactionID(transactionID : String , res: @escaping (String) -> Void){
        
        var request = URLRequest(url: URL(string: "\(base_url)/payment/transactionID/")!)
            request.httpMethod = "POST"
        let postString = "secret_id=\(secret_id)&secret_key=\(secret_key)&transaction_ID=\(transactionID)"
            request.httpBody = postString.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
                           
        guard let data = data, error == nil else {
            print("error=(error)")
            return
        }

        if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
            print("statusCode should be 200, but is \(httpStatus.statusCode)")
            print("response = \(String(describing: response))")

        }

        let responseString = String(data: data, encoding: .utf8)
        struct param : Codable {
                var amount: String?
                var transactionid: String?
                var description: String?
                var status: String?
                private enum CodingKeys : String, CodingKey {
                    case amount = "Amount "
                    case transactionid = "Transaction ID "
                    case description = "Description "
                    case status = "Status Payment "
                }
        }
        let jsonData = responseString!.data(using: .utf8)!
        let users = try! JSONDecoder().decode([param].self, from: jsonData)
            for user in users {
                print(user.status)
                print(user.transactionid)
                print(user.description)
                print(user.amount)
            }
            
            res(responseString!)
           
        }
        
    
        task.resume()
         
        
    }
    
    
    func CheckOrderID(orderID : String,  res: @escaping (String) -> Void){
        
        var request = URLRequest(url: URL(string: "\(base_url)/payment/OrderID/")!)
            request.httpMethod = "POST"
        let postString = "secret_id=\(secret_id)&secret_key=\(secret_key)&order_id=\(orderID)"
            request.httpBody = postString.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
                           
        guard let data = data, error == nil else {
            print("error=(error)")
            return
        }

        if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
            print("statusCode should be 200, but is \(httpStatus.statusCode)")
            print("response = \(String(describing: response))")

        }

        let responseString = String(data: data, encoding: .utf8)
        struct param : Codable {
                var amount: String?
                var order_id: String?
                var description: String?
                var status: String?
                private enum CodingKeys : String, CodingKey {
                    case amount = "Amount "
                    case order_id = "Order ID "
                    case description = "Description "
                    case status = "Status Payment "
                }
        }
        let jsonData = responseString!.data(using: .utf8)!
        let users = try! JSONDecoder().decode([param].self, from: jsonData)
            for user in users {
                print(user.status)
                print(user.order_id)
                print(user.description)
                print(user.amount)
            }
            
            res(responseString!)
           
        }
        
    
        task.resume()
        
    }

}
