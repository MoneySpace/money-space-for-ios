
# Requirements  
 - Swift 4+

# Intallation
 - Download repository
 - Unzip file
 - Add MoneyspacePayment.swift to project
# Configuration

#### Paste this code and enter your secret id and secret key for call functions.

```swift
var ms = MoneyspacePayment(secret_id: "",secret_key: "")
```  
# Example  
 
#### CreateTransactionID

```swift
ms.CreateTransactionID(
	firstname: "test", // Show firstname on receipt
	lastname: "1234", // Show lastname on receipt
	email: "", // Email for receipt
	phone: "", 
	amount: "1.57", // Amount to be paid ( Must be 2 decimal place and no comma in numbers. )
	description: "", // Product or service description
	address: "", // Customer address
	feeType: "include", // who pays fees  , Merchant : include & Shopper  : exclude ( qrprom can't use exclude )
	message: "", // instructions to merchant
	order_id: "", // order id , Uppercase letters,number and cannot be repeated ( Maximum 20 characters )
	payment_type: "card", // card : Card 3D secured , qrnone : Pay by QR Code Promptpay , qrnone : Image QR Code
	success_Url: "", // As payment success, it will  redirect  to the specific  url.
	fail_Url: "", // As payment failed, it will  redirect  to the specific  url.
	cancel_Url: "", // As payment canceled, it will  redirect  to the specific  url.
){(result) in
	print("Created : "  + result)
}
```  
*****

#### CheckTransactionID
```swift
ms.CheckTransactionID(
	transactionID: "Enter your transactionID")
{(result) in
	print("transactionID : "  + result) // Show Status Payment , Amount
}
```
*****

#### CheckOrderID
```swift
ms.CheckOrderID(
	orderID: "Enter your orderID")
{(result) in
	print("OrderID : "  + result) // Show Status Payment , Amount
}
```  
*****

# Changelog

- 2021‑09‑20 : Updated example
- 2019‑11‑30 : Add new QR Code Promptpay and example code
- 2019-11-23 : Update README
- 2019-11-14 : Added