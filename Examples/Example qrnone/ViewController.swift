//
//  ViewController.swift
//  demoshop
//
//  Created by smind on 3/9/2562 BE.
//  Copyright © 2562 stepclick. All rights reserved.
//

import UIKit
import SafariServices


class ViewController: UIViewController {
    
    
    @IBOutlet weak var imgqr: UIImageView!
    
    var ms = MoneyspacePayment(secret_id: "",secret_key: "")
    
    var transaction_ID : String = "";
    
  
    @IBAction func onButtonClick(_ sender: Any) {
        
        
        self.showToast(message: "Create QR...")

        ms.CreateTransactionID(
            firstname: "test",
            lastname: "1234",
            email: "",
            phone: "",
            amount: "2.57",
            description: "",
            address: "",
            feeType: "include", // include , exclude
            message: "",
            order_id: "ex0033".uppercased(),
            payment_type: "qrnone", // card , qrnone
        ){(result) in
           
            print(result)
            
            do{
                if let json = result.data(using: String.Encoding.utf8){
                    if let jsonData = try JSONSerialization.jsonObject(with: json, options: .allowFragments) as? [String:AnyObject]{
                        
                        let status = jsonData["status"] as? String
                        
                        if status == "success"{
                        
                            let TransactionID = jsonData["TransactionID"] as! String
                            let image_qrprom = jsonData["image_qrprom"] as! String
                            let qrcode = jsonData["qrcode"] as! String
                        
                            self.transaction_ID = TransactionID;
                        
                            self.imgqr.image = UIImage(named: "limm")
                            if let url = NSURL(string: image_qrprom) {
                                if let data = NSData(contentsOf: url as URL){
                                    self.imgqr.contentMode = UIView.ContentMode.scaleAspectFit;
                                    self.imgqr.image = UIImage(data: data as Data)
                                
                                }
                            }
                            
                        }else if status == "error create"{
                            print(result)
                            self.showToast(message: "Create Error")
                        }
                    
                    }
                }
            }catch {
                print(error.localizedDescription)
                
            }
            


        }
        
//        ms.CheckTransactionID(
//            transactionID: self.transaction_ID)
//        {(result) in
//            print(result)
//        }
//        ms.CheckOrderID(
//            orderID: "")
//        {(result) in
//            print("OrderID : " + result)
//        }



        
    }
    
    @IBAction func checktransaction(_ sender: Any) {
        if self.transaction_ID != ""{
            ms.CheckTransactionID(
                transactionID: self.transaction_ID)
            {(result) in
                
                print(result)
                
            }
        }else{
            self.showToast(message: "Not found transaction_ID")
        }
    }
    
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.9, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    }


}

