//
//  ViewController.swift
//  demoshop
//
//  Created by smind on 3/9/2562 BE.
//  Copyright © 2562 stepclick. All rights reserved.
//

import UIKit
import SafariServices


class ViewController: UIViewController {
    
    var ms = MoneyspacePayment(secret_id: "",secret_key: "")
    

    @IBAction func onButtonClick(_ sender: Any) {
        
        
        ms.CreateTransactionID(
            firstname: "test", // Show firstname on receipt
            lastname: "1234", // Show lastname on receipt
            email: "", // Email for receipt
            phone: "", // 
            amount: "1.57", // Amount to be paid ( Must be 2 decimal place and no comma in numbers. )
            description: "", // Product or service description
            address: "", // Customer address
            feeType: "include", // who pays fees  , Merchant : include & Shopper  : exclude ( qrprom can't use exclude )
            message: "", // instructions to merchant
            order_id: "", // order id ( Not be able to duplicated and Maximum 20 characters )
            payment_type: "card", // card : Card 3D secured , qrnone : Pay by QR Code Promptpay
            success_Url: "", // As payment success, it will  redirect  to the specific  url.
            fail_Url: "", // As payment failed, it will  redirect  to the specific  url.
            cancel_Url: "", // As payment canceled, it will  redirect  to the specific  url.
        ){(result) in
            print("Created : " + result)
        }
        
        ms.CheckTransactionID(
            transactionID: "")
        {(result) in
            print("transactionID : " + result)
        }
        ms.CheckOrderID(
            orderID: "")
        {(result) in
            print("OrderID : " + result)
        }


        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


}

